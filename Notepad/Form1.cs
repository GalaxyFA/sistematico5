﻿using Notepad.Secuencial;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Notepad
{
    public partial class FrmPrincipal : Form
    {
        public FrmPrincipal()
        {
            InitializeComponent();
        }

        private void FrmPrincipal_Load(object sender, EventArgs e)
        {

        }

        private void nuevoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Formulario f = new Formulario();
            f.MdiParent = this;
            f.Show();

        }

        private void abrirToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog.ShowDialog();
        /*   
            string filepath = saveFileDialog.FileName;
            SecuencialStream ss = new SecuencialStream(filepath);
            Form activeChild = this.ActiveMdiChild;
            TextBox text = (TextBox)activeChild.Controls[0];
            ss.writeText(text.Text);*/
        }

        private void guardarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int count = this.MdiChildren.Length;
            if(count == 0)
            {
                return;
            }
            DialogResult result = saveFileDialog.ShowDialog();
            if(result== DialogResult.OK)
            {
                string filepath = saveFileDialog.FileName;
                SecuencialStream ss = new SecuencialStream(filepath);
                Form activeChild = this.ActiveMdiChild;
                TextBox text = (TextBox)activeChild.Controls[0];
                ss.writeText(text.Text);



            }
        }
    }
}
