﻿using NETProjectTutorial.entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    class ClienteModelo
    {
        private static List<Cliente> clientes = new List<Cliente>();

        public static List<Cliente> GetClientes()
        {
            return clientes;
        }
        public static void Popular()
        {
            clientes = JsonConvert.DeserializeObject<List<Cliente>>(System.Text.Encoding.Default.GetString(NETProjectTutorial.Properties.Resources.Cliente_datos));
        }
    }
}
