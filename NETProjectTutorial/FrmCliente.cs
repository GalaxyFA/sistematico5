﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmCliente : Form
    {
        private DataTable tblCliente;
        private DataSet dsCliente;
        private BindingSource bsCliente;
        private DataRow drCliente;
        public FrmCliente()
        {
            InitializeComponent();
            bsCliente = new BindingSource();
        }

        public DataRow DrCliente
        {
            set
            {
                drCliente = value;
                txtCedula.Text = drCliente["Cedula"].ToString();
                txtNombre.Text = drCliente["Nombre"].ToString();
                txtApellido.Text = drCliente["Apellido"].ToString();
                txtTelefono.Text = drCliente["Telefono"].ToString();
                txtCorreo.Text = drCliente["Direccion"].ToString();
            }
        }

        public DataTable TblCliente
        {
            get
            {
                return tblCliente;
            }

            set
            {
                tblCliente = value;
            }
        }

        public DataSet DsCliente
        {
            get
            {
                return dsCliente;
            }

            set
            {
                dsCliente = value;
            }
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            string cedula, nombre, apellido, telefono, correo, direccion;

            cedula = txtCedula.Text;
            nombre = txtNombre.Text;
            apellido = txtApellido.Text;
            telefono = txtTelefono.Text;
            correo = txtCorreo.Text;
            direccion = txtDireccion.Text;
            
            if(drCliente != null)
            {
                DataRow drplus = TblCliente.NewRow();

                int indice = TblCliente.Rows.IndexOf(drCliente);
                drplus["Id"] = drCliente["Id"];
                drplus["Cedula"] = cedula;
                drplus["Nombre"] = nombre;
                drplus["Apellido"] = apellido;
                drplus["Telefono"] = telefono;
                drplus["Correo"] = correo;
                drplus["Direccion"] = direccion;

                TblCliente.Rows.RemoveAt(indice);
                TblCliente.Rows.InsertAt(drplus, indice);
            }else
            {
                TblCliente.Rows.Add(TblCliente.Rows.Count + 1, cedula, nombre, apellido, telefono, correo, direccion);
            }
            Dispose();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void FrmCliente_Load(object sender, EventArgs e)
        {
            bsCliente.DataSource = DsCliente;
            bsCliente.DataMember = DsCliente.Tables["Clientes"].TableName;
        }
    }
}
