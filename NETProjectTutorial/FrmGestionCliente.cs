﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmGestionCliente : Form
    {
        private DataSet dsCliente;
        private BindingSource bsCliente;

        public DataSet DsCliente
        {
            get
            {
                return dsCliente;
            }

            set
            {
                dsCliente = value;
            }
        }

        public FrmGestionCliente()
        {
            InitializeComponent();
            bsCliente = new BindingSource();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            FrmCliente fc = new FrmCliente();
            fc.TblCliente = DsCliente.Tables["Clientes"];
            fc.DsCliente = DsCliente;
            fc.ShowDialog();
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowC = dataGridView1.SelectedRows;

            if(rowC.Count == 0)
            {
                MessageBox.Show(this, "Erro, Fila no seleccionada", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            

        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {

        }

        private void dataGridVw1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void FrmGestionCliente_Load(object sender, EventArgs e)
        {
            bsCliente.DataSource = DsCliente;
            bsCliente.DataMember = DsCliente.Tables["Clientes"].TableName;
            dataGridView1.DataSource = bsCliente;
            dataGridView1.AutoGenerateColumns = true;
        }

        

        private void textSearcher_TextChanged(object sender, EventArgs e)
        {

            try
            {
                bsCliente.Filter = string.Format("Cedula like '*{0}*' or Nombre like '*{0}*' or Apellido like '*{0}*'", textSearcher.Text);

            }
            catch (InvalidExpressionException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
